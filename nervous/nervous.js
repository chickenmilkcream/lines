var blip;
var bugs = [];
var score = 0;
var centered_width;
var centered_height;
var random_colours = [];
var bug_types = ['random', 'sym', 'bounce', 'spin'];
/*
Order of function calls in ColourBug - run() causes colours to changes everything
*/

function preload() {
    blip = loadSound("blip.wav");
    shake = loadSound("shake.wav");
    bap = loadSound("bap.wav");
    cow = loadSound("cow.wav");
}

function setup() {
    // Window and background Setup
    createCanvas(windowWidth,windowHeight);
    // frameRate(4);
    // Center everything
    centered_width = width/2;
    centered_height = height/2;
    background(50, 89, 100);
    // Generate 6 random colours
    for (let i = 0; i < 6; i++) {
        random_colours.push(random(1, 266));
    }
}
        
function draw() {
    // Score logic (we always start with one)
    var num_bugs = 1 + Math.floor(score/100)
    while (bugs.length < num_bugs){
        var type = bug_types[Math.floor(Math.random() * bug_types.length)];
        switch (type) {
            case 'random':
                bugs.push(new RandomBug(random_colours));
                break;
            case 'sym':
                bugs.push(new SymBug(random_colours));
                break;
            case 'bounce':
                bugs.push(new BounceBug(random_colours));
                break;
            case 'spin':
                bugs.push(new SpinBug(random_colours));
                break;
        }
    }
    // Center the coordinate system
    translate(centered_width, centered_height);
    // Draw every bug
    background(100, 100, 100);
    for(var i = 0; i < bugs.length; i++){
        bugs[i].run();
    }
    // Get the score text and width
    var sc = score;
    // TODO: Make this better
    // rect(5, 15, textWidth(sc) + 10, 20);
    // stroke(0);
    // fill(0);
    // Draw the score
    textSize(24);
    stroke(0);
    fill(255);
    text(sc, -centered_width + 10, -centered_height + 30);
}

function generate_pos_or_neg() {
    x = [1, -1];
    return x[Math.floor(Math.random() * x.length)]
}

// ------ CLASSES ------

class TheBug{
    /* Is a bug that starts at a random position
    Properties:
        x - x position of bug
        y - y position of bug
    Methods:
        run - calls move and display
        move - not implemented, moves the bug
        display - draws the bug on the screen
        wrap_around - wraps the bug around if it goes to far
        reenter_point - calculates where the bug should come out on wrap
        update_score - increases the score by n
    */
    constructor(){
        this.x = random(-centered_width, centered_width);
        this.y = random(-centered_height, centered_height);
        this.radius = random(5, 15); 
        this.speed = 0;
        this.r_side = centered_width + this.radius 
        this.l_side = -(this.r_side) 
        this.b_side = centered_height + this.radius 
        this.t_side = -(this.b_side) 
    }
    run() {;
        this.move();
        this.display();
    }
    
    move() {}

    display() {
        this.wrap_around();
        var diameter = this.radius * 2;
        ellipse(this.x, this.y, diameter, diameter);
    }
    
    wrap_around() {
        // If you go off right side or the left side of the screen 
        if(this.x > this.r_side || this.x < this.l_side){
            this.x = this.reenter_x(this.x);
        }
        // If you go off the bottom or the the top of the screen
        if (this.y > this.b_side || this.y < this.t_side) {
            this.y = this.reenter_y(this.y);
        }
        
    }

    reenter_x(x) {
        if(x > this.r_side){
            return this.l_side;                
        }
        if(x < this.l_side){
            return this.r_side;
        }        
    }

    reenter_y(y){
        if (y > this.b_side) {
            return this.t_side;
        }        
        if (y < this.t_side) {
            return this.b_side;
        }
    }

    update_score(n) {
        score += n;
    }
}

class ColourBug extends TheBug {
    /*
    A reactive coloured bug that has a random start 
        Properties
            colour - the current colour of the bug
            h_colour_list - happy colours of the bug
            a_colour_list - angry colours of the bug
            rage -  a copy of the hapy bug colours
        Methods:
            run - runs cooldown_ colours and check_touched
            cooldown_colours - calms bug down by reverting to original colour
            check_touched - checks if a bug has been touched
            on_touched - when the bug is being pressed he gets mad
            get_mad - makes bug angry and changes colour
    */
    constructor(colour_list) {
        super()
        // Points earned by touching
        this.point = 1
        // Colour Stuff
        this.colour = {};
        var h_colour_list = [];
        var a_colour_list = [];
        for (var i = 0; i < 6; i += 2) {
            var diff = 60;
            h_colour_list.push(colour_list[i] + random(-diff, diff));
            a_colour_list.push(colour_list[i + 1] + random(-diff, diff));
        }
        this.colour['happy'] = h_colour_list;
        this.colour['angry'] = a_colour_list;
        this.rage = this.colour['happy'].slice();
    }

    run() {
        this.check_touched();
        this.cooldown_colours();
        super.run();
    }

    cooldown_colours(){
        var happy_list = [this.colour['happy'][0], this.colour['happy'][1], this.colour['happy'][2]];
        // Updates Colours
        for(var i = 0; i < 3; i++) {                
            if (this.rage[i] < happy_list[i]) {
                // Increase towards goal        
                this.rage[i]++;
            } else if(this.rage[i] > happy_list[i]) {
                // Decrease towards goal
                this.rage[i]--;                   
            }             
        }        
        fill(this.rage[0], this.rage[1], this.rage[2]);
    }

    check_touched() {        
        var centered_mouseX = mouseX - centered_width
        var centered_mouseY = mouseY - centered_height
        if (dist(centered_mouseX, centered_mouseY, this.x, this.y) < (this.radius)){
            this.on_touched()
        }     
    }
    
    on_touched() {
        this.get_mad();
        this.make_noise();
        this.update_score(this.point);
    }

    make_noise() {}

    get_mad() {
        var mad_colours = this.colour['angry'];
        this.rage[0] = mad_colours[0];
        this.rage[1] = mad_colours[1];
        this.rage[2] = mad_colours[2];
        fill(this.rage[0],this.rage[1],this.rage[2]);
    }
}

class BounceBug extends ColourBug {
    /* This is a color bug that bounces of the walls and moves quickly
    */
   constructor(colour_list) {
        super(colour_list);        
        this.x = random(-centered_width + this.radius, centered_width - this.radius)
        this.y = random(-centered_height + this.radius, centered_height - this.radius)
        this.point = 10;
        // Required for touching function
        this.original_radius = random(5, 15); 
        this.radius = this.original_radius;
        this.speedlim = 10;
        this.speed_x = generate_pos_or_neg() * random(3,this.speedlim);
        this.speed_y = generate_pos_or_neg() * random(3,this.speedlim);
        this.bounce_wall_r = this.r_side - (2 * this.radius)
        this.bounce_wall_l = this.l_side + (2 * this.radius)
        this.bounce_wall_b = this.b_side - (2 * this.radius)
        this.bounce_wall_t = this.t_side + (2 * this.radius)
   }
   display() {
        this.bounce();
        var diameter = this.radius * 2;
        ellipse(this.x, this.y, diameter, diameter);
   }
    move() {
        super.move();
        this.x += this.speed_x;
        this.y += this.speed_y;
    }
    make_noise() {
        bap.play();
    }
    wrap_around() {
        // We won't be wrapping
    }
    bounce() {
        // If you go off right side or the left side of the screen 
        if(this.x >= this.bounce_wall_r || this.x <= this.bounce_wall_l){
            this.speed_x = -this.speed_x;
        }
        // If you go off the bottom or the the top of the screen
        if (this.y >= this.bounce_wall_b || this.y <= this.bounce_wall_t) {
            this.speed_y = -this.speed_y;
        }
        
    }
}

class RandomBug extends ColourBug {
    /*This is a bug that reacts when hovered over with the mouse

    Properties:
        original_raidus: Stores the original radius of this bug
        speed_lim: Stores the original speed of this bug

    Methods:
        run (overridden): apply the sedative every frame
        apply_adrenaline: causes bug to move more randomly and increases size
        apply_sedative: calms the movement back down
        move (overridden): the bug performs a random walk

    */
    constructor(colour_list) {            
        super(colour_list);        
        this.point = 2;
        // Required for touching function
        this.original_radius = random(5, 15); 
        this.radius = this.original_radius;
        this.speed = 4;
        this.speedlim = 4;
    }

    run() {
        super.run();
        this.apply_sedative();
    }
    
    move() {
        super.move();
        this.x += random(-this.speed,this.speed);
        this.y += random(-this.speed,this.speed);
    }

    on_touched(){
        super.on_touched();
        this.apply_adrenaline();
    }

    make_noise() {
        shake.play();
    }
    
    apply_adrenaline() {
        this.speed++;
        this.radius += 1;
    }

    apply_sedative() {
        this.speed = max(this.speedlim, this.speed - .05);
        this.speed = max(this.speedlim, this.speed - .05);
        this.radius = max(this.original_radius, this.radius - .2);
    }

}

// TODO: Add different colours for this bug
// TODO: Fix hitbox, or not, its pretty close
// TODO: Make movement more interesting.
class SymBug extends RandomBug {
    /*
    This bug has a friend, and you get more points from him
        Properties
                x2 - set to a random width
                y2 - set to a random height          
            Methods:
                move (OVR) - moves itself in unison with its friend
                display (EXT) - recentres the square
                touched (OVR) -  checks to see if either bugs are being touched           
    */
    
    constructor(colour_list) {
        super(colour_list);
        this.point = 1
        this.x2 = random(centered_width);
        this.y2 = random(centered_height);        
    }

    move() {
        // So the movement is symmetrical
        var x_jitter = random(-this.speed,this.speed)
        var y_jitter = random(-this.speed,this.speed)
        this.x += x_jitter;
        this.y += y_jitter;
        this.x2 = -this.x;
        this.y2 = -this.y;
    }
    display() {
        super.display()
        // recenter square
        var d = this.radius * 2
        line(this.x , this.y , this.x2 , this.y2);
        rect(this.x - d/2, this.y - d/2, d, d);
        rect(this.x2 - d/2, this.y2 - d/2, d, d);
    }

    wrap_around() {
        super.wrap_around();
        // If you go off right side or the left side of the screen 
        if(this.x2 > this.r_side || this.x2 < this.l_side){
            this.x2 = this.reenter_x(this.x2);
        }
        // If you go off the bottom or the the top of the screen
        if (this.y2 > this.b_side || this.y2 < this.t_side) {
            this.y2 = this.reenter_y(this.y2);            
        }
    }
    check_touched() {        
        var centered_mouseX = mouseX - centered_width
        var centered_mouseY = mouseY - centered_height
        if (dist(centered_mouseX, centered_mouseY, this.x, this.y) < (this.radius) || dist(centered_mouseX, centered_mouseY, this.x2, this.y2) < (this.radius)){
            this.on_touched()
        }     
    }

    on_touched() {
        super.on_touched()
        // Check if it's at least inside one of the symbugs        
        var inside = dist(mouseX, mouseY, this.x, this.y) < (this.radius) || dist(mouseX, mouseY, this.x2, this.y2) < (this.radius);      
        if (inside){
            this.apply_adrenaline();
        }     
    }
    make_noise() {
        blip.play();
    }
    on_touched() {
        super.on_touched()
    }
}

class SpinBug extends ColourBug {    
    /*    
    TODO: Figure out why rotation speed also changes the rotation radius
    A normal bug, but it rotates and moves slower and in a random predifined direction
        Properties
                rotation_radius - radius of the rotation circle 
                rotation_speed - speed of rotation
                theta - angle of rotation (in radians)
                speed_x - the speed on the x axis   
                speed_y - the speed on the x axis   
                sca_x -  
                central_x - the x coordinate of the point that is being rotated around   
                central_y - the y coordinate of the point that is being rotated around 
            Methods:
                move (OVR) - controls the bugs circle path 
                increase_sporadicness_and_size - TBD
                apply_sedative - slows down the bug         
    */
    constructor(colour_list) {
        super(colour_list);
        this.point = 5;
        this.rotation_radius = random(min(centered_width/5, centered_height/5), min(centered_width,centered_height));
        // this.rotation_speed = TWO_PI/60;
        this.circumference = TWO_PI * (this.rotation_radius);
        this.rotation_speed = generate_pos_or_neg() * (90/this.rotation_radius) * (TWO_PI / 60);        
        this.rotation_speed_lim = this.rotation_speed;
        this.theta = 0;
        // Controls the rotation
        // this.sca_x = ceil(random(1,10));
        // this.sca_y = ceil(random(1,10));
        this.sca_x = 1;
        this.sca_y = 1;
    }
    move() {
        // this.x = this.x + this.direction.x + cos(this.theta) * this.rotation_radius;
        // this.y = this.y + this.direction.y + sin(this.theta) * this.rotation_radius;        
        var rot_x = cos(this.sca_x * this.theta) * this.rotation_radius;
        var rot_y = sin(this.sca_y * this.theta) * this.rotation_radius;
        this.x = rot_x;
        this.y = rot_y;
        this.theta += this.rotation_speed;        
    }
    run() {
        super.run();
        this.apply_sedative()
    }

    on_touched() {
        super.on_touched();
        this.apply_adrenaline();
    }

    make_noise() {
        cow.play();
    }
    apply_adrenaline() {
        if (this.rotation_speed >= 0) {
            this.rotation_speed += .055;        
        } else {
            this.rotation_speed -= .055;        
        }
        
    }
    apply_sedative() {
        if (this.rotation_speed >= 0) {
            this.rotation_speed = max(this.rotation_speed_lim, this.rotation_speed - .005);
        } else {
            this.rotation_speed = min(this.rotation_speed_lim, this.rotation_speed + .005);
        }
    }
    
    
}

function windowResized() {
    waitress = millis() + 2000;
    if (fullscreen()) {
        resizeCanvas(displayWidth, displayHeight);
        viewfs.style.display = "none";
        exitfs.style.display = "block";
    } else {
        resizeCanvas(windowWidth,windowHeight);
        exitfs.style.display = "none";
        viewfs.style.display = "block";
    }
    cursor();  
    showing = true;
}