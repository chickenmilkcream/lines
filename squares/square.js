var bap_played = false;
var blip_played = false;

function preload() { 
    bap = loadSound("bap.wav");
    blip = loadSound("blip.wav");
}

function setup() {
    // Window and background Setup
    createCanvas(windowWidth,windowHeight);
    // frameRate(4);
    background(0);
    rectMode(CENTER);
}

function draw() {
    var inverseX = width - mouseX;
    var inverseY = (height - mouseY) * .5;
    if (inverseY > mouseY) {
        if (!bap_played) {
            bap.play();
            bap_played = true;
            blip_played = false;
        }
        background(255);
        stroke(0);
        fill(125);
        rect(mouseX, height/2, mouseY, mouseY);
        fill(255, 0, 0);
        rect(inverseX, height/2, inverseY, inverseY);
        conn_sq(mouseX, mouseY, inverseX, inverseY);
    } else {
        if (!blip_played) {
            blip.play();
            blip_played = true;
            bap_played = false;
        }
        background(0);
        stroke(255);
        fill(255, 0, 0);
        rect(inverseX, height/2, inverseY, inverseY);
        fill(125);
        rect(mouseX, height/2, mouseY, mouseY);
        conn_sq(inverseX, inverseY, mouseX, mouseY);
    }
}

function conn_sq(x, y, xi, yi) {
    // Fix this later
    line(x + y * .5, height/2 + y * .5, xi + yi * .5, height/2 + yi * .5);
    line(x - y * .5, height/2 + y * .5, xi - yi * .5, height/2 + yi * .5);
    line(x + y * .5, height/2 - y * .5, xi + yi * .5, height/2 - yi * .5);
    line(x - y * .5, height/2 - y * .5, xi - yi * .5, height/2 - yi * .5);
}

