
function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
}
 
function draw() {
    if (mouseIsPressed) {
        fill((mouseX * .1) % 255, (mouseY*.1) % 255, ((mouseX + mouseY) * 0.1) % 255);
        stroke((mouseX * .1) % 255, (mouseY*.1) % 255, ((mouseX + mouseY) * 0.1) % 255);
    } else {
        background((mouseX * .1) % 255, (mouseY*.1) % 255, ((mouseX + mouseY) * 0.1) % 255);
        fill(255);
    }
    
    var o_midpoints = midpoint(0, 0, mouseX, mouseY);
    var tr_midpoints = midpoint(windowWidth, 0, mouseX, mouseY);
    var br_midpoints = midpoint(windowWidth, windowHeight, mouseX, mouseY);
    var bl_midpoints = midpoint(0, windowHeight,mouseX, mouseY);

    line(windowWidth, 0, mouseX, mouseY);
    line(0, 0, mouseX, mouseY);
    line(0, windowHeight, mouseX, mouseY);
    line(windowWidth, windowHeight, mouseX, mouseY);    

    ellipse(o_midpoints[0], o_midpoints[1], 10, 10);
    ellipse(mouseX + tr_midpoints[0], tr_midpoints[1], 10, 10);
    ellipse(mouseX + br_midpoints[0], mouseY + br_midpoints[1], 10, 10);
    ellipse(bl_midpoints[0], mouseY + bl_midpoints[1], 10, 10);
    ellipse(mouseX,mouseY, mouseX * .1 % 150, (mouseY *.1) % 150);

    line(o_midpoints[0], o_midpoints[1],mouseX + tr_midpoints[0], tr_midpoints[1]);
    line(mouseX + tr_midpoints[0], tr_midpoints[1], mouseX + br_midpoints[0], mouseY + br_midpoints[1]);
    line(mouseX + br_midpoints[0], mouseY + br_midpoints[1], bl_midpoints[0], mouseY + bl_midpoints[1]);
    line(bl_midpoints[0], mouseY + bl_midpoints[1], o_midpoints[0], o_midpoints[1]);    

    console.log(o_midpoints[0],o_midpoints[1]);
} 

function midpoint(x1, y1, x2, y2) {
    return [abs((x2 - x1)/2), abs((y2 - y1)/2)]
}

 
function windowResized() {
  waitress = millis() + 2000;
  if (fullscreen()) {
    resizeCanvas(displayWidth, displayHeight);
    viewfs.style.display = "none";
    exitfs.style.display = "block";
  } else {
    resizeCanvas(windowWidth,windowHeight);
    exitfs.style.display = "none";
    viewfs.style.display = "block";
  }
  cursor();  
  showing = true;
  background(0);
}