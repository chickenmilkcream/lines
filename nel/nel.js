var x = 0;
var y = 0;
var radius = 50;
var num_circ = 100;
var circ_list = [];

function setup() {
    createCanvas(windowWidth,windowHeight);
    background(0);
    for (let i = 0; i < num_circ; i++) {
        circ_list.push(new SoundCircle(random(0, .3), random(50, width/8)));
    }
}

function draw() {
    background(0);
    for (let i = 0; i < num_circ; i++) {
        circ_list[i].run();
        print(print(circ_list[i].x))
    }
}

class SoundCircle {
    constructor(easing, radius) {
        this.easing = easing;
        this.radius = radius;
        this.color = [floor(random(0, 256)), 
            floor(random(0, 256)), 
            floor(random(0, 256))]
        this.targetX = 0;
        this.targetY = 0;
        this.x = random(0, width);
        this.y = random(0, height);
    }
    run() {
        this.react();
        ellipse(this.x, this.y, this.radius);
    }

    follow() {
        this.targetX = mouseX;
        this.targetY = mouseY;
        this.x += (this.targetX - this.x) * this.easing;
        this.y += (this.targetY - this.y) * this.easing;
    }
    repel() {
        this.targetX = mouseX;
        this.targetY = mouseY;
        var repel_x_factor = (this.targetX - this.x) * this.easing
        var repel_y_factor = (this.targetY - this.y) * this.easing
        print(repel_x_factor);
        this.x -= repel_x_factor;
        this.y -= repel_y_factor;
    }
    react() {
        if (mouseIsPressed) {
            var colors = this.color;
            fill(colors[0], colors[1], colors[2]);
            this.repel();
        } else {
            fill(255,255,255);
            this.follow();
        }
    }
}