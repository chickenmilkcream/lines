var x;
var y;
var surface;
var theta = 0;
var pop = 200;

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    background(0);
    x = 0;
    y = 0;
    surface = generate_surface(0);
}

function generate_surface(intensity) {
    // var numPovars = int(map(mouseX, 0, width, 1, 60));
    var outsideRadius = min(width/2, height/2);
    var layers = 8;
    var layer_width = outsideRadius/ layers;
    var layer_list = [];
    var xoff = 0;
    var yoff = 0;
    var numPovars = 25;
    var angle = 0;
    var angleStep = 180.0/numPovars;
    var surface = [];
    insideRadius = min(width/2, height/2);

    for (var i = 0; i <= outsideRadius; i += layer_width){
        layer_list.push(i)
    }

    for (var k = 0; k < layers; k += 1) {
        for (var i = 0; i <= numPovars; i++) {
            var px = x + cos(radians(angle)) * layer_list[k + 1];
            var py = y + sin(radians(angle)) * layer_list[k + 1];
            angle += angleStep;
            surface.push([px, py, map(noise(xoff, yoff),0,1, -intensity, intensity)]);
            px = x + cos(radians(angle)) * layer_list[k];
            py = y + sin(radians(angle)) * layer_list[k];
            surface.push([px, py, map(noise(xoff, yoff),0,1, -intensity, intensity)]);
            angle += angleStep;
            xoff += .05;
        }
        yoff += .05;
  }
  return surface
}

function draw() {
    rotateX(theta);
    rotateY(theta);
    background(0);


    surface = generate_surface(map(mouseX, 0, width, -400, 400));
    // surface = generate_surface(map(sin(frameCount * 0.01), -1, 1, -400, 400));
    mcolour_r = map(mouseY, 0, height, 0, 255);
    mcolour_g = map(mouseY, height, 0, 0, 255);
    mcolour_b = map(mouseX, 0, height, 0, 255);
    fill(mcolour_r, mcolour_g, mcolour_b);

    beginShape(TRIANGLE_STRIP); 
    for (var i = 0; i < surface.length; i ++) {
        vertex(surface[i][0], surface[i][1], surface[i][2])
    } 
    endShape();
    theta += .004;
}